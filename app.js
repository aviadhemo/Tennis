/**
 * Created by aviad on 19/03/2017.
 */
var canvas;
var canvasContext;
var ballX = 50;
var ballSpeedX = 10;
var ballY = 50;
var ballSpeedY = 4;

var player1Score = 0;
var computerScore = 0;

var showingWinScreen = false;

var paddle1Y = 250;
var paddle2Y = 250;
const PADDLE_HEIGHT = 100;
const PADDLE_WIDTH = 10;
const WINNING_SCORE = 3;

function handleMouseClick(event) {
    if(showingWinScreen){
        player1Score = 0;
        computerScore = 0;
        showingWinScreen = false;
    }
}

window.onload = function () {
    canvas = document.getElementById('gameCanvas');
    canvasContext = canvas.getContext('2d');

    var framesPerSecond = 30;
    setInterval(function () {
        movePaddles();
        drawCanvasSkeleton();
    }, 1000/framesPerSecond);

    canvas.addEventListener('mousedown', handleMouseClick);

    canvas.addEventListener('mousemove',
            function (event) {
                var mousrPosition = calculationMousePosition(event);
                paddle1Y = mousrPosition.y - (PADDLE_HEIGHT/2);

            })
}

function AIMovement() {
    var paddle2Center =  paddle2Y + (PADDLE_HEIGHT/2);
    // if(paddle2Y < ballY - 35) {
    //     paddle2Y += 6;
    // } else if(paddle2Y > ballY + 35){
    //     paddle2Y -= 6;
    // }
    paddle2Y = ballY - PADDLE_HEIGHT/2;
}

function movePaddles() {
    if(showingWinScreen){
        return;
    }
    AIMovement();

    ballX += ballSpeedX;
    ballY += ballSpeedY;

    if(ballX < 0){
        if(ballY > paddle1Y && ballY < paddle1Y+PADDLE_HEIGHT){
            ballSpeedX = - ballSpeedX;
            var deltaY =  ballY - (paddle1Y + PADDLE_HEIGHT/2);
            ballSpeedY = deltaY * 0.35;
        }else{
            computerScore++; //must be BEFORE the reset ball
            ballReset();
        }
    }
    if(ballX > canvas.width){
        if(ballY > paddle2Y && ballY < paddle2Y+PADDLE_HEIGHT){
            ballSpeedX = - ballSpeedX;
            var deltaY =  ballY - (paddle2Y + PADDLE_HEIGHT/2);
            ballSpeedY = deltaY * 0.35;
        }else{
            player1Score++; //must be BEFORE the reset ball
            ballReset();
        }
    }

    if(ballY < 0){
        ballSpeedY = -ballSpeedY;
    }
    if(ballY > canvas.height){
        ballSpeedY = -ballSpeedY;
    }
}

function drawNet() {
    for(var i = 0;i < canvas.height; i+=40){
        colorRectangle(canvas.width/2 - 1, i, 2, 20, 'white');
    }
}

function drawCanvasSkeleton() {
    //draws the black canvas
    colorRectangle(0, 0, canvas.width, canvas.height, 'black');

    if(showingWinScreen){
        if(player1Score >= WINNING_SCORE){
            canvasContext.fillStyle = 'yellow';
            canvasContext.fillText("Player 1 won", 350, 200)
        }
            if(computerScore >= WINNING_SCORE){
                canvasContext.fillStyle = 'yellow';
                canvasContext.fillText("Computer won", 350, 200)
            }
        canvasContext.fillStyle = 'yellow';
        canvasContext.fillText("click to continue", 350, 500)
        return;
    }

    drawNet();

    //this is the left player paddle
    colorRectangle(0, paddle1Y, PADDLE_WIDTH, PADDLE_HEIGHT, 'white');

    //this is the right computer player paddle
    colorRectangle(canvas.width - PADDLE_WIDTH, paddle2Y, PADDLE_WIDTH, PADDLE_HEIGHT, 'white');

    //draw the ball
    colorBall(ballX, ballY, 10, 'white');

    canvasContext.fillText(player1Score, 100, 100);
    canvasContext.fillText(computerScore,canvas.width - 100, 100);
}

function colorRectangle(leftX, topY, width, height, drawColor) {
    canvasContext.fillStyle = drawColor;
    canvasContext.fillRect(leftX, topY, width, height);
}

function colorBall(centerX, centerY, radius, color) {
    canvasContext.beginPath();
    canvasContext.arc(centerX, centerY, radius, 0, Math.PI*2, true);
    canvasContext.fill();
}

function calculationMousePosition(event) {
    var rectangle = canvas.getBoundingClientRect();
    var root = document.documentElement;
    var mouseX = event.clientX - rectangle.left - root.scrollLeft;
    var mouseY = event.clientY - rectangle.top - root.scrollTop;

    return{
        x:mouseX,
        y:mouseY
    };
}

function ballReset() {
    if(player1Score >= WINNING_SCORE || computerScore >= WINNING_SCORE){
        showingWinScreen = true;
    }
    ballSpeedX = -ballSpeedX;
    ballX = canvas.width/2;
    ballY = canvas.height/2;
}